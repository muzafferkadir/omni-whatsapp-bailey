require('dotenv').config();

const checkApiKey = async (req, res, next) => {
  if (req.headers['x-api-key'] && process.env.API_KEY === req.headers['x-api-key']) {
    return next();
  }
  return res.status(401).send('X-API-Key is invalid');
};

module.exports = checkApiKey;
