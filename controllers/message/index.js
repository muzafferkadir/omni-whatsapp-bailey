const sendText = require('./send-text');
const sendAudio = require('./send-audio');
const sendImage = require('./send-image');
const sendVideo = require('./send-video');
const sendDocument = require('./send-document');
const sendButton = require('./send-button');

const checkApiKey = require('../../middlewares/check-api-key');

module.exports = (app) => {
  app.use('/message/send-text', checkApiKey, sendText);
  app.use('/message/send-audio', checkApiKey, sendAudio);
  app.use('/message/send-image', checkApiKey, sendImage);
  app.use('/message/send-video', checkApiKey, sendVideo);
  app.use('/message/send-document', checkApiKey, sendDocument);
  app.use('/message/send-button', checkApiKey, sendButton);
};
