const express = require('express');

const router = express.Router();

const Client = require('../../models/client');

router.post('/:id', async (req, res) => {
  try {
    const {
      to, url, mimetype, filename,
    } = req.body;

    if (!to || !url || !mimetype) {
      res.status(400).send('to, mimetype and url fields are required');
      return;
    }

    const client = await Client.findById(req.params.id);
    if (!client || !client.connected) {
      res.status(404).send('Client is not found or client is not connected');
      return;
    }

    const indexOfSocket = global.sockets.findIndex((skt) => skt.clientId === client.id);
    const socket = global.sockets[indexOfSocket];

    if (!socket) {
      res.status(400).send('Client is not connected');
      return;
    }

    await socket.sendMessage(`${to}@s.whatsapp.net`, { document: { url }, mimetype, filename });

    res.status(200).send('Message send');
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
