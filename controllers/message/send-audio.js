const express = require('express');

const router = express.Router();

// const Fs = require('fs');
// const Path = require('path');
// const Axios = require('axios');
const Client = require('../../models/client');

// async function downloadImage(url) {
//   const path = Path.resolve(__dirname, 'images', 'code.jpg');
//   const writer = Fs.createWriteStream(path);

//   const response = await Axios({
//     url,
//     method: 'GET',
//     responseType: 'stream',
//   });

//   response.data.pipe(writer);

//   return new Promise((resolve, reject) => {
//     writer.on('finish', resolve);
//     writer.on('error', reject);
//   });
// }

router.post('/:id', async (req, res) => {
  try {
    const { to, url, mimetype } = req.body;

    // const file = await downloadImage('https://unsplash.com/photos/AaEQmoufHLk/download?force=true');

    if (!to || !url || !mimetype) {
      res.status(400).send('All fields are required');
      return;
    }

    const client = await Client.findById(req.params.id);
    if (!client || !client.connected) {
      res.status(404).send('Client is not found or client is not connected');
      return;
    }

    const indexOfSocket = global.sockets.findIndex((skt) => skt.clientId === client.id);
    const socket = global.sockets[indexOfSocket];

    if (!socket) {
      res.status(400).send('Client is not connected');
      return;
    }

    await socket.sendMessage(`${to}@s.whatsapp.net`, { audio: { url }, mimetype });

    res.status(200).send('Message send');
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
