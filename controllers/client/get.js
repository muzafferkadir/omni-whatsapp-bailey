const express = require('express');

const router = express.Router();
const Client = require('../../models/client');

router.get('/:id', async (req, res) => {
  try {
    const client = await Client.findById(req.params.id);
    if (!client) {
      res.status(404).send('Clients is not found');
      return;
    }

    res.status(200).send(client);
  } catch (err) {
    res.status(500).send(err);
    console.log(err);
  }
});

module.exports = router;
