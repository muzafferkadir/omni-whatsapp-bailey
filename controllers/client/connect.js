const express = require('express');

const router = express.Router();
const Client = require('../../models/client');

const connectToWhatsApp = require('../../services/connect-whatsapp');

router.post('/:id', async (req, res) => {
  try {
    const client = await Client.findById(req.params.id);
    if (!client) {
      res.status(404).send('Client is not found');
    }

    if (client.connected) {
      res.status(400).send('Client is already connected');
      return;
    }

    const qr = await connectToWhatsApp(req.params.id);

    res.status(200).send({ qr });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

module.exports = router;
