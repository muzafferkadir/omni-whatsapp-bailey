const express = require('express');

const router = express.Router();
const Client = require('../../models/client');
const connectToWhatsApp = require('../../services/connect-whatsapp');

router.post('/:id', async (req, res) => {
  try {
    const client = await Client.findById(req.params.id);
    if (!client) {
      res.status(404).send('Client is not found');
    }

    if (!client.last_session) {
      res.status(404).send('Last session is not found');
    }

    const reconnect = await connectToWhatsApp(req.params.id, client.last_session, false);

    res.status(200).send({ reconnect });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

module.exports = router;
