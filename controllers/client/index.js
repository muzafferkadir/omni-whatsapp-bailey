const create = require('./create');
const get = require('./get');
const connect = require('./connect');
const connectMulti = require('./connect-multi');
const reconnect = require('./reconnect');
// const reconnectExample = require('./reconnect-example');
// const reconnectMulti = require('./reconnect-multi');
const destroy = require('./delete');
const list = require('./list');

const checkApiKey = require('../../middlewares/check-api-key');

module.exports = (app) => {
  app.get('/', (req, res) => {
    res.render('index', { title: 'Express' });
  });
  app.use('/create', checkApiKey, create);
  app.use('/show', checkApiKey, get);
  app.use('/connect', checkApiKey, connect);
  app.use('/connect-multi', checkApiKey, connectMulti);
  app.use('/reconnect', checkApiKey, reconnect);
  // app.use('/reconnect-multi', checkApiKey, reconnectExample);
  app.use('/delete', checkApiKey, destroy);
  app.use('/list', checkApiKey, list);
};
