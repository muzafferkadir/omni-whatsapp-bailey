const express = require('express');

const router = express.Router();
const Client = require('../../models/client');

router.get('/', async (req, res) => {
  try {
    const clients = await Client.find();
    if (!clients) {
      res.status(404).send('Clients are not found');
    }

    res.status(200).send(clients);
  } catch (err) {
    res.status(500).send(err);
    console.log(err);
  }
});

module.exports = router;
