const express = require('express');

const router = express.Router();
const Client = require('../../models/client');

router.delete('/:id', async (req, res) => {
  try {
    await Client.findByIdAndDelete(req.params.id);

    res.status(201).send('Client is deleted');
  } catch (err) {
    res.status(500).send(err);
    console.log(err);
  }
});

module.exports = router;
