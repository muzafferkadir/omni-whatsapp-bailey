const P = require('pino');
const makeWASocket = require('@adiwajshing/baileys').default;

const { DisconnectReason, makeInMemoryStore, useSingleFileAuthState } = require('@adiwajshing/baileys');

const express = require('express');

const router = express.Router();

// the store maintains the data of the WA connection in memory
// can be written out to a file & read from it
const store = makeInMemoryStore({ logger: P().child({ level: 'debug', stream: 'store' }) });
store.readFromFile('./baileys_store_multi.json');
// save every 10s
setInterval(() => {
  store.writeToFile('./baileys_store_multi.json');
}, 10_000);

const { state, saveState } = useSingleFileAuthState('./auth_info_multi.json');

// start a connection
const startSock = () => {
  const sock = makeWASocket({
    logger: P({ level: 'trace' }),
    printQRInTerminal: true,
    auth: state,
    version: [2, 2204, 13],
  });

  store.bind(sock.ev);
  sock.ev.on('messages.update', (m) => console.log(m));

  sock.ev.on('connection.update', (update) => {
    const { connection, lastDisconnect } = update;
    if (connection === 'close') {
      // reconnect if not logged out
      if (lastDisconnect.error?.output?.statusCode !== DisconnectReason.loggedOut) {
        startSock();
      } else {
        console.log('connection closed');
      }
    }

    console.log('connection update', update);
  });
  // listen for when the auth credentials is updated
  sock.ev.on('creds.update', saveState);

  return sock;
};

router.post('/', async () => {
  startSock();
});

module.exports = router;
