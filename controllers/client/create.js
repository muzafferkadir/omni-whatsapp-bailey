const express = require('express');

const router = express.Router();
const Client = require('../../models/client');

router.post('/', async (req, res) => {
  try {
    if (!req.body.name || !req.body.webhook) {
      res.status(400).send('Name and webhook is required!');
      return;
    }

    // to do add joi
    const newClient = await Client.create({ ...req.body });

    res.status(201).send(newClient);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router;
