/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */

const { makeWALegacySocket, downloadContentFromMessage } = require('@adiwajshing/baileys');
const makeWASocket = require('@adiwajshing/baileys').default;
const axios = require('axios');
const { v4: uuid } = require('uuid');
const mime = require('mime-types');
const Client = require('../models/client');
const s3 = require('./s3');

// Connect QR requests
const QR_RETRY_NUMBER = 2;
const UPLOAD_FOLDER = 'whatsapp-baileys/chats';

// Session Parse Functions
const replacer = (k, value) => {
  if (Buffer.isBuffer(value) || value instanceof Uint8Array || value?.type === 'Buffer') {
    return { type: 'Buffer', data: Buffer.from(value?.data || value).toString('base64') };
  }

  return value;
};

const reviver = (_, value) => {
  if (typeof value === 'object' && !!value && (value.buffer === true || value.type === 'Buffer')) {
    const val = value.data || value.value;
    return typeof val === 'string' ? Buffer.from(val, 'base64') : Buffer.from(val);
  }

  return value;
};

const sendMessageToWebhook = async (clientId, m) => {
  const client = await Client.findById(clientId);
  const messageType = Object.keys(m?.messages[0]?.message)[0] || 'text';

  let stream;
  let buffer = Buffer.from([]);
  let url;
  const filename = `${UPLOAD_FOLDER}/${m.messages[0].key.remoteJid.split('@')[0]}/${uuid()}.${mime.extension(m.messages[0].message[messageType].mimetype)}`;

  switch (messageType) {
    case 'imageMessage':
      if (m.type === 'notify') return;

      stream = await downloadContentFromMessage(m.messages[0].message.imageMessage, 'image');
      for await (const chunk of stream) {
        buffer = Buffer.concat([buffer, chunk]);
      }

      url = await s3.upload(filename, buffer);

      await axios.post(client.webhook, { ...m, url });
      break;

    case 'videoMessage':
      if (m.type === 'notify') return;

      stream = await downloadContentFromMessage(m.messages[0].message.videoMessage, 'video');
      for await (const chunk of stream) {
        buffer = Buffer.concat([buffer, chunk]);
      }

      url = await s3.upload(filename, buffer);

      await axios.post(client.webhook, { ...m, url });
      break;

    case 'documentMessage':
      if (m.type === 'notify') return;

      stream = await downloadContentFromMessage(m.messages[0].message.documentMessage, 'document');
      for await (const chunk of stream) {
        buffer = Buffer.concat([buffer, chunk]);
      }

      url = await s3.upload(filename, buffer);

      await axios.post(client.webhook, { ...m, url });
      break;

    case 'audioMessage':
      if (m.type === 'notify') return;

      stream = await downloadContentFromMessage(m.messages[0].message.audioMessage, 'audio');
      for await (const chunk of stream) {
        buffer = Buffer.concat([buffer, chunk]);
      }

      url = await s3.upload(filename, buffer);

      await axios.post(client.webhook, { ...m, url });
      break;

    default:
      await axios.post(client.webhook, m);
      break;
  }
};

// Connect with legacy version
async function connectLegacy(clientId, timeout = 10000) {
  return new Promise((resolve, reject) => {
    const socketOptions = {
      connectTimeoutMs: 30 * 1000, // 30s
      keepAliveIntervalMs: 20 * 1000, // 20s
      printQRInTerminal: true,
    };

    const WA = makeWALegacySocket(socketOptions);

    WA.clientId = clientId;
    WA.numberOfTry = 1;
    global.sockets.push(WA);

    WA.ev.on('connection.update', async (update) => {
      try {
        const { connection } = update;

        const indexOfSocket = global.sockets.findIndex((skt) => skt.clientId === clientId);
        const socket = global.sockets[indexOfSocket];

        const client = await Client.findById(clientId);

        if (connection === 'close') {
          socket.ev.removeAllListeners();
          global.sockets[indexOfSocket] = null;
          global.sockets.splice(indexOfSocket, 1);

          client.connected = false;
          await client.save();

          await axios.post(client.webhook, update?.lastDisconnect?.error?.output);
        } else if (connection === 'open') {
          client.connected = true;
          await client.save();

          await axios.post(client.webhook, update);
        }

        if (!connection) {
          if (socket.numberOfTry < QR_RETRY_NUMBER) {
            socket.numberOfTry += 1;
          } else {
            socket.ev.removeAllListeners();
            global.sockets[indexOfSocket] = null;
            global.sockets.splice(indexOfSocket, 1);
          }
        }

        if (update.qr) {
          resolve(update.qr);
        }
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('messages.upsert', async (m) => {
      try {
        await sendMessageToWebhook(clientId, m);
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('creds.update', async (creds) => {
      const client = await Client.findById(clientId);

      client.last_session = JSON.stringify(creds, replacer, 2);
      await client.save();
    });

    setTimeout(() => {
      reject(new Error('Timeout waiting for QR'));
    }, timeout);
  });
}

// Try reconnect with legacy version
async function reconnectLegacy(clientId, lastSession, timeout) {
  return new Promise((resolve, reject) => {
    const result = JSON.parse(lastSession, reviver);

    const WA = makeWALegacySocket({
      auth: result,
    });

    WA.clientId = clientId;
    WA.numberOfTry = 1;
    global.sockets.push(WA);

    WA.ev.on('connection.update', async (update) => {
      try {
        const { connection } = update;

        const indexOfSocket = global.sockets.findIndex((skt) => skt.clientId === clientId);
        const socket = global.sockets[indexOfSocket];

        const client = await Client.findById(clientId);

        if (connection === 'close') {
          socket.ev.removeAllListeners();
          global.sockets[indexOfSocket] = null;
          global.sockets.splice(indexOfSocket, 1);

          client.connected = false;
          await client.save();

          await axios.post(client.webhook, update?.lastDisconnect?.error?.output);
        } else if (connection === 'open') {
          client.connected = true;
          await client.save();

          await axios.post(client.webhook, update);
          resolve(update);
        }

        if (!connection) {
          if (socket.numberOfTry < QR_RETRY_NUMBER) {
            socket.numberOfTry += 1;
          } else {
            socket.ev.removeAllListeners();
            global.sockets[indexOfSocket] = null;
            global.sockets.splice(indexOfSocket, 1);
          }
        }
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('messages.upsert', async (m) => {
      try {
        await sendMessageToWebhook(clientId, m);
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('creds.update', async (creds) => {
      const client = await Client.findById(clientId);

      client.last_session = JSON.stringify(creds, replacer, 2);
      await client.save();
    });

    setTimeout(() => {
      reject(new Error('Timeout for waiting reconnect. Please connect device again!'));
    }, timeout);
  });
}

// Connect with legacy version
async function connectMulti(clientId, timeout = 10000) {
  return new Promise((resolve, reject) => {
    const socketOptions = {
      connectTimeoutMs: 10 * 1000, // 30s
      keepAliveIntervalMs: 5 * 1000, // 20s
      printQRInTerminal: true,
      version: [2, 2204, 13],
    };

    const WA = makeWASocket(socketOptions);

    WA.clientId = clientId;
    WA.numberOfTry = 1;
    global.sockets.push(WA);

    WA.ev.on('connection.update', async (update) => {
      try {
        const { connection } = update;

        const indexOfSocket = global.sockets.findIndex((skt) => skt.clientId === clientId);
        const socket = global.sockets[indexOfSocket];

        const client = await Client.findById(clientId);

        if (connection === 'close') {
          socket.ev.removeAllListeners();
          global.sockets[indexOfSocket] = null;
          global.sockets.splice(indexOfSocket, 1);

          client.connected = false;
          await client.save();

          await axios.post(client.webhook, update?.lastDisconnect?.error?.output);
        } else if (connection === 'open') {
          client.connected = true;
          await client.save();

          await axios.post(client.webhook, update);
        }

        if (!connection) {
          if (socket.numberOfTry < QR_RETRY_NUMBER) {
            socket.numberOfTry += 1;
          } else {
            socket.ev.removeAllListeners();
            global.sockets[indexOfSocket] = null;
            global.sockets.splice(indexOfSocket, 1);
          }
        }

        if (update.qr) {
          resolve(update.qr);
        }
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('messages.upsert', async (m) => {
      try {
        const client = await Client.findById(clientId);

        await axios.post(client.webhook, m);
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('creds.update', async (creds) => {
      console.log(creds);
      const client = await Client.findById(clientId);

      client.last_session = JSON.stringify(creds, replacer, 2);
      console.log(typeof client.last_session);
      await client.save();
    });

    setTimeout(() => {
      reject(new Error('Timeout waiting for QR'));
    }, timeout);
  });
}

// Try reconnect with legacy version
async function reconnectMulti(clientId, lastSession, timeout) {
  return new Promise((resolve, reject) => {
    const result = JSON.parse(lastSession, reviver);
    console.log(result);

    const WA = makeWASocket({
      auth: result,
      version: [2, 2204, 13],
    });

    WA.clientId = clientId;
    WA.numberOfTry = 1;
    global.sockets.push(WA);

    WA.ev.on('connection.update', async (update) => {
      try {
        const { connection } = update;

        const indexOfSocket = global.sockets.findIndex((skt) => skt.clientId === clientId);
        const socket = global.sockets[indexOfSocket];

        const client = await Client.findById(clientId);

        if (connection === 'close') {
          socket.ev.removeAllListeners();
          global.sockets[indexOfSocket] = null;
          global.sockets.splice(indexOfSocket, 1);

          client.connected = false;
          await client.save();

          await axios.post(client.webhook, update?.lastDisconnect?.error?.output);
        } else if (connection === 'open') {
          client.connected = true;
          await client.save();

          await axios.post(client.webhook, update);
          resolve(update);
        }

        if (!connection) {
          if (socket.numberOfTry < QR_RETRY_NUMBER) {
            socket.numberOfTry += 1;
          } else {
            socket.ev.removeAllListeners();
            global.sockets[indexOfSocket] = null;
            global.sockets.splice(indexOfSocket, 1);
          }
        }
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('messages.upsert', async (m) => {
      try {
        const client = await Client.findById(clientId);

        await axios.post(client.webhook, m);
      } catch (error) {
        console.log(error);
      }
    });

    WA.ev.on('creds.update', async (creds) => {
      const client = await Client.findById(clientId);

      client.last_session = JSON.stringify(creds, replacer, 2);
      console.log(typeof client.last_session);
      await client.save();
    });

    setTimeout(() => {
      reject(new Error('Timeout for waiting reconnect. Please connect device again!'));
    }, timeout);
  });
}

function connectToWhatsApp(clientId, lastSession = null, whatsappBeta = false, timeout = 10000) {
  if (lastSession && !whatsappBeta) { return reconnectLegacy(clientId, lastSession, timeout); }

  if (!lastSession && whatsappBeta) { return connectMulti(clientId, timeout); }

  if (lastSession && whatsappBeta) { return reconnectMulti(clientId, lastSession, timeout); }

  return connectLegacy(clientId, timeout);
}

module.exports = connectToWhatsApp;
