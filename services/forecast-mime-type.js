const mime = require('mime-types');

module.exports = async (mediaUrl) => {
  if (!mediaUrl) return '';

  const url = new URL(mediaUrl);

  return await mime.lookup(`${url.origin}${url.pathname}`) || '';
};
