const { v4: uuid } = require('uuid');
const mime = require('mime-types');
const S3 = require('./s3').RemoteUploader;
const forecastMimeType = require('./forecast-mime-type');

const upload = async (context) => {
  const mediaUrl = context.url;

  if (!mediaUrl) return null;

  const mimeType = await forecastMimeType(mediaUrl);

  const filename = `${context.folder}/${context.receiverId}/${uuid()}.${mime.extension(mimeType)}`;
  const uploader = new S3(filename, mediaUrl);
  const data = await uploader.dispatch();

  return data.Location;
};

module.exports = {
  upload,
};
