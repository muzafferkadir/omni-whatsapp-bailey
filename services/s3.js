const AWS = require('aws-sdk');

const S3 = new AWS.S3({ apiVersion: '2006-03-01' });
const axios = require('axios');
const stream = require('stream');

AWS.config.update({ region: process.env.AWS_REGION });

class RemoteUploader {
  constructor(filename, url, options = {}) {
    this.url = url;
    this.stream = stream;
    this.axios = axios;
    this.s3 = S3;
    this.filename = filename;
    this.options = options;
    this.content_type = 'application/octet-stream';
    this.uploadStream();
  }

  uploadStream() {
    const pass = new this.stream.PassThrough();
    this.promise = this.s3.upload({
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: this.filename,
      ACL: 'public-read',
      Body: pass,
      ContentType: this.content_type,
    }).promise();
    return pass;
  }

  initiateAxiosCall() {
    const config = {
      method: 'get',
      url: this.url,
      responseType: 'stream',
    };

    if (this.options.axios) Object.assign(config, this.options.axios);

    return axios(config);
  }

  async dispatch() {
    await this.initiateAxiosCall().then((response) => {
      if (response.status === 200) {
        this.content_type = response.headers['content-type'];
        response.data.pipe(this.uploadStream());
      }
    });
    return this.promise;
  }
}

const actions = {
  async upload(filename, content, options = {}) {
    const params = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: filename,
      ACL: 'public-read',
      Body: content,
    };

    Object.assign(params, options);

    try {
      const data = await S3.upload(params).promise();

      if (data) return data.Location;
    } catch (err) {
      return err;
    }
    return null;
  },
};

module.exports = {
  upload: actions.upload,
  RemoteUploader,
};
