require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const mongoose = require('mongoose');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Controllers
require('./controllers/client')(app);

// Message Controllers
require('./controllers/message')(app);

// DB
const mongoDB = process.env.DB_URL;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

global.sockets = [];

const Client = require('./models/client');

// Reset All clients
Client.updateMany(
  { },
  { connected: false },
  (err, docs) => {
    if (err) {
      console.log(err);
    } else {
      console.log('Updated Docs : ', docs);
    }
  },
);
// setInterval(() => {
//   try {
//     console.log(JSON.stringify(global.sockets));
//   } catch (error) {
//     console.log(error);
//   }
// }, 10_000);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
