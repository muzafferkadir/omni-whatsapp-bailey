const mongoose = require('mongoose');

const { Schema } = mongoose;

const clientSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  webhook: {
    type: String,
    required: true,
  },
  wa_id: {
    type: String,
  },
  last_session: {
    type: Object,
  },
  qr: {
    type: String,
  },
  connected: {
    type: Boolean,
    default: false,
  },
}, {
  timestamps: true,
});

module.exports = mongoose.model('Client', clientSchema);
